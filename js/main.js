(function ($) {
    "use strict";

    /* Enabling support for new HTML5 tags for IE6, IE7 and IE8 */
    if(navigator.appName == 'Microsoft Internet Explorer' ){
        if( ( navigator.userAgent.indexOf('MSIE 6.0') >= 0 ) || ( navigator.userAgent.indexOf('MSIE 7.0') >= 0 ) || ( navigator.userAgent.indexOf('MSIE 8.0') >= 0 ) ){
            document.createElement('header')
            document.createElement('nav')
            document.createElement('section')
            document.createElement('aside')
            document.createElement('footer')
            document.createElement('article')
            document.createElement('hgroup')
            document.createElement('figure')
            document.createElement('figcaption')
            document.createElement('video')
        }
    }
    
  

})(jQuery);

jQuery(document).ready(function()
{
		
	$('.owl-one').owlCarousel({
		margin:30,
		smartSpeed:450,
		loop:true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		dots:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true,
				dots:false,
			},
			768:{
				items:1,
				nav:true,
			},
			1000:{
				items:1,
				nav:true,
				loop:true,
			}
		}
	});
	$('.owl-two').owlCarousel({
		animateOut: 'hinge',
		animateIn: 'fadeInRightBig',
		margin:30,
		smartSpeed:450,
		loop:true,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		dots:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:2,
				nav:true,
				dots:false,
			},
			768:{
				items:3,
				nav:true,
			},
			1000:{
				items:4,
				nav:true,
				loop:true,
			}
		}
	});	
	$('.owl-three').owlCarousel({
		margin:30,
		smartSpeed:450,
		loop:true,
		autoplay:false,
		autoplayTimeout:5000,
		autoplayHoverPause:true,
		dots:false,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
				nav:true,
				dots:false,
			},
			768:{
				items:1,
				nav:true,
			},
			1000:{
				items:1,
				nav:true,
				loop:true,
			}
		}
	});	



});


$("#spy a[href^='#']").on('click', function(e) {

	// prevent default anchor click behavior
	e.preventDefault();

	// animate
	$('html, body').animate({
		scrollTop: $(this.hash).offset().top-89
	  }, 800, function(){

		// when done, add hash to url
		// (default click behaviour)
		window.location.hash = this.hash;
	  });

 });